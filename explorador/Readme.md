# Explorador

Implementación del explorador el cual permite visualizar una lista de componentes léxicos si así lo requiere el usuario.
El archivo no se accede desde la carpta actual, se debe de acceder desde prolactosa.py, ubicada en la carpte anterior a la actual.

## Propósito de archivos

-AtributosComponente.py: Clase que almacena la información de los atributos adicionales de un componente léxico.
-ComponenteLexico.py: Clase que almacena la información de un componente léxico.
-DescripcionComponente: Clase que funciona como diccionario para dar una descripcion a cada lexema.
-DescriptoresComponentes: Clase que declara los diferentes tipos de componentes que un lexema puede tomar (declarado en regex).
-explorador.py: Archivo principal del explorador de prolactosa. 
-TipoComponente.py: Clase que declara los ENUM's que utilizara prolactosa correspondientes a sus tipos de componentes.