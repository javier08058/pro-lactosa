#Declaracion de diccionario para descripciones de tokens

# Importacion de archivos y librerias
from explorador.TipoComponente import TipoComponente

class DescripcionComponente:
    """
    Clase que funciona como diccionario para dar una descripcion a cada lexema

    Algunos tipos de lexema que pueden ser muy distintos entre si mismos se les da una descripcion generica, mientras que los que siempre son iguales uno especifico
    """
    def evaluar_token(mitoken, mi_componente):
        AtributosComponente = {

            'sea_necio': 'equivalente a While',
            'si_pasa': 'Si condicional',
            'tal_vez': 'No condicional',
            'agregue': 'equivalente a and',
            'intente_esta': 'equivalente a or',
            'al_chile': 'equivalente a True',
            'no_invente': 'equivalente a False',
            '+': 'Operador logico de suma + ',
            '-': 'Operador logico de resta - ',
            '*': 'Operador logico de multiplicacion * ',
            '/': 'Operador logico de division / ',
            '=':'Conector de un identificador y un valor',
            'igual': 'Igual comparativo ',
            'diferente': 'Diferente comparativo',
            'menor':'Menor comparativo',
            'mayor':'Mayor comparativo',
            'menor_igual':'Menor o igual comparativo',
            'mayor_igual':'Mayor o igual comparativo',
            'Muestre': 'imprime cualquier variable o texto ',
            'Mezclar_ingredientes': 'Este te concatena 2 textos',
            'Mimir':'Espera x cantidad de tiempo ',
            'Ingrese':'Entrada por medio de consola',
            'monchar': 'Retorno un valor',
            'inicio': 'Inicio de funcion.',
            'fin': 'Fin de la funcion.',
        }
        """
        Revisa que el token exista en la lista, en caso de ser variables o identificadores
        retorna el mismo mensaje.
        """
        if mitoken not in AtributosComponente:
            if mi_componente is TipoComponente.IDENTIFICADOR:
                return 'Esto es un identificador'
            if mi_componente is TipoComponente.TEXTO:
                return 'Esto es un texto'
            if mi_componente is TipoComponente.ENTERO:
                return 'Esto es un numero entero'
            if mi_componente is TipoComponente.PUNTUACION:
                return 'Esto es un simbolo del sistema'
            if mi_componente is TipoComponente.FLOTANTE:
                return 'Esto es un flotante'
            return 'Identificadores o variables'
        # si el token si existe, busco y traigo el atributo.
        else:
            return AtributosComponente.get(mitoken)