# Implementa el veficador de ciruelas

from typing import List
from utils.árbol import ÁrbolSintáxisAbstracta, NodoÁrbol, TipoNodo
from utils.tipo_datos import TipoDatos
from verificador.TablaSimbolos import TablaSímbolos
from verificador.Visitante import Visitante

class Verificador:

    asa            : ÁrbolSintáxisAbstracta
    visitador      : Visitante
    tabla_símbolos : TablaSímbolos

    def __init__(self, nuevo_asa: ÁrbolSintáxisAbstracta):

        self.asa            = nuevo_asa

        self.tabla_símbolos = TablaSímbolos()
        ##self.__cargar_ambiente_estándar()

        self.visitador      = Visitante(self.tabla_símbolos)

    def imprimir_asa(self):
        """
        Imprime el árbol de sintáxis abstracta
        """
            
        if self.asa.raiz is None:
            print([])
        else:
            self.asa.imprimir_preorden()

    def __cargar_ambiente_estándar(self):
        ###Muestre|Ingrese|Mezclar_ingredientes|Mimir
        funciones_estandar = [ ('Muestre', TipoDatos.NINGUNO),
                ('Ingrese', TipoDatos.TEXTO),
                ('Mimir', TipoDatos.NINGUNO),
                ('Mezclar_ingredientes', TipoDatos.TEXTO)]

        for nombre, tipo in  funciones_estandar:
            nodo = NodoÁrbol(TipoNodo.FUNCION, contenido=nombre, atributos= {'tipo': tipo})
            self.tabla_símbolos.nuevo_registro(nodo)
    
    def verificar(self):
        self.visitador.visitar(self.asa.raiz)
    
    def tiene_errores(self):
        if self.tabla_símbolos.lista_errores != []:
            return True
        return False 

    def imprimir_errores(self):
        ''' 
        Funcion para imprimir los errores que detectó el verificador
        '''
        print('\n','Lista de Errores Verificador:')
        for error in self.tabla_símbolos.lista_errores:
            print('\n','Error #'+str(self.tabla_símbolos.lista_errores.index(error)+1))
            print(error)
        print('\n','Fin de lista de Errores')






