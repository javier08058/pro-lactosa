class TablaSímbolos:
    """ 
    Almacena información auxiliar para decorar el árbol de sintáxis
    abstracta con información de tipo y alcance.

    La estructura de símbolos es una lista de diccionarios 
    """
    símbolos: list = []
    profundidad : int  = 0
    recuperandose_error: bool = False
    lista_errores: list = []

    def manejar_errores(self, componente):
        """
        La funcion se encarga de manejar el componente no encontrado y salta a la siguiente funcion,
        si la bandera de errores está levantada, se ignora
        """
        if(not self.recuperandose_error):
            self.lista_errores.append(f'<Error: El valor {componente.texto} en la fila {componente.atributos_adicionales.fila}, en la columna {componente.atributos_adicionales.columna} no se encuentra declarado.')
            self.recuperandose_error = True

    def abrir_bloque(self):
        """
        Inicia un bloque de alcance (scope)
        """
        self.profundidad += 1

    def cerrar_bloque(self):
        """
        Termina un bloque de alcance y al acerlo elimina todos los
        registros de la tabla que estan en ese bloque
        """

        for registro in self.símbolos:
            if registro['profundidad'] == self.profundidad:
                self.símbolos.remove(registro)

        self.profundidad -= 1

    def nuevo_registro(self, nodo):
        """
        Introduce un nuevo registro a la tabla de símbolos
        """
        # El nombre del identificador + el nivel de profundidad 

        """
        Los atributos son: nombre, profundidad, referencia

        referencia es una referencia al nodo dentro del árbol
        (Técnicamente todo lo 'modificable (mutable)' en python es una
        referencia siempre y cuando use la POO... meh... más o menos.
        """

        diccionario = {}

        diccionario['nombre']      = nodo.contenido.texto 
        diccionario['profundidad'] = self.profundidad
        diccionario['referencia']  = nodo

        self.símbolos.append(diccionario)

    def verificar_existencia(self, componente):
        """
        Verficia si un identificador existe cómo variable/función global o local
        """
        nombre = componente.texto
        for registro in self.símbolos:
            #print(registro['nombre'])
            #print(nombre)
            # si existe
            if registro['nombre'] == nombre and \
                    registro['profundidad'] <= self.profundidad:
                return registro

        #si no existe entonces se reporta error
        self.manejar_errores(componente)

        return None

    def __str__(self):

        resultado = 'TABLA DE SÍMBOLOS\n\n'
        resultado += 'Profundidad: ' + str(self.profundidad) +'\n\n'
        for registro in self.símbolos:
            resultado += str(registro) + '\n'

        return resultado