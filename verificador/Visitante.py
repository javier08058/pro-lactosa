from verificador.TablaSimbolos import TablaSímbolos
from utils.árbol import ÁrbolSintáxisAbstracta, NodoÁrbol, TipoNodo
from utils.tipo_datos import TipoDatos
class Visitante:

    tabla_símbolos: TablaSímbolos

    def __init__(self, nueva_tabla_símbolos):
        self.tabla_símbolos = nueva_tabla_símbolos 

    def visitar(self, nodo:NodoÁrbol):
        """
        Este método es necesario por que uso un solo tipo de nodo para
        todas las partes del árbol por facilidad... pero cómo lo hice
        tuanis allá... pues bueno... acá hay que pagar el costo.
        """
        
        if nodo.tipo is TipoNodo.PROGRAMA:
            self.__visitar_programa(nodo)

        elif nodo.tipo is TipoNodo.ASIGNACION:
            self.__visitar_asignación(nodo)

        elif nodo.tipo is TipoNodo.EXPRESIÓN_MATEMÁTICA:
            self.__visitar_expresión_matemática(nodo)

        elif nodo.tipo is TipoNodo.EXPRESIÓN:
            self.__visitar_expresión(nodo)

        elif nodo.tipo is TipoNodo.FUNCION:
            self.__visitar_función(nodo)

        elif nodo.tipo is TipoNodo.INVOCACION:
            self.__visitar_invocación(nodo)

        elif nodo.tipo is TipoNodo.PARAMETROS_INVOCACION:
            self.__visitar_parámetros_invocación(nodo)

        elif nodo.tipo is TipoNodo.PARAMETROS_FUNCION:
            self.__visitar_parámetros_función(nodo)

        elif nodo.tipo is TipoNodo.INSTRUCCION:
            self.__visitar_instrucción(nodo)

        elif nodo.tipo is TipoNodo.REPETICION:
            self.__visitar_repetición(nodo)

        elif nodo.tipo is TipoNodo.BIFURCACION:
            self.__visitar_bifurcación(nodo)

        elif nodo.tipo is TipoNodo.SISUCEDE:
            self.__visitar_sisucede(nodo)

        elif nodo.tipo is TipoNodo.SINO:
            self.__visitar_sino(nodo)
        
        ##elif nodo.tipo is TipoNodo.OPERADOR_LOGICO:
            ##self.__visitar_operador_lógico(nodo)

        elif nodo.tipo is TipoNodo.CONDICION:
            self.__visitar_condición(nodo)

        elif nodo.tipo is TipoNodo.COMPARACION:
            self.__visitar_comparación(nodo)

        elif nodo.tipo is TipoNodo.RETORNO:
            self.__visitar_retorno(nodo)

        elif nodo.tipo is TipoNodo.PRINCIPAL:
            self.__visitar_principal(nodo)

        elif nodo.tipo is TipoNodo.BLOQUE_INSTRUCCIONES:
            self.__visitar_bloque_instrucciones(nodo)

        elif nodo.tipo is TipoNodo.OPERADOR:
            self.__visitar_operador(nodo)

        elif nodo.tipo is TipoNodo.VERACIDAD:
            self.__visitar_veracidad(nodo)

        elif nodo.tipo is TipoNodo.COMPARADOR:
            self.__visitar_comparador(nodo)

        elif nodo.tipo is TipoNodo.TEXTO:
            self.__visitar_texto(nodo)

        elif nodo.tipo is TipoNodo.ENTERO:
            self.__visitar_entero(nodo)

        elif nodo.tipo is TipoNodo.FLOTANTE:
            self.__visitar_flotante(nodo)

        elif nodo.tipo is TipoNodo.IDENTIFICADOR:
            self.__visitar_identificador(nodo)

        elif nodo.tipo is TipoNodo.AMBIENTE_ESTANDAR:
            self.__visitar_ambiente_estandar(nodo)

        else:
            # Puse esta opción nada más para que se vea bonito... 
            raise Exception('En realidad nunca va a llegar acá')
    
    
    def manejar_errores(self, nodo, tipoError):
        """
        La funcion se encarga de manejar el componente no encontrado y salta a la siguiente funcion,
        si la bandera de errores está levantada, se ignora
        """
        tipo = nodo.tipo
        componente = nodo.contenido
        if(not self.tabla_símbolos.recuperandose_error):
            if tipoError == TipoNodo.INVOCACION:
                texto = f'<Error: El valor {componente.texto} en la fila {componente.atributos_adicionales.fila}, en la columna {componente.atributos_adicionales.columna} deberia ser de tipo funcion pero es de tipo {tipo.name}'
            elif tipoError == TipoNodo.PARAMETROS_INVOCACION:
                texto =  f'<Error: El valor {componente.texto} en la fila {componente.atributos_adicionales.fila}, en la columna {componente.atributos_adicionales.columna} deberia ser de una variable pero es de tipo {tipo.name}'

            self.tabla_símbolos.lista_errores.append(texto)
            self.tabla_símbolos.recuperandose_error = True

    def __visitar_ambiente_estandar(self, nodo_actual:NodoÁrbol):
        """
        Muestre (Texto)                     : Imprime el texto en la pantalla
        Ingrese(Texto)                      : Pide al usuario que ingrese un texto
        Mezclar_ingredientes(Texto1, Texto2): Junta dos textos y devuelve uno
        Mimir(Número)                       : Espera X tiempo para continuar
        """
        
        # Meto la función en la tabla de símbolos (IDENTIFICACIÓN)
        for nodo in nodo_actual.nodos:
            #print(nodo)
            # Verifico que exista si es un identificador (IDENTIFICACIÓN)
            if nodo.tipo == TipoNodo.IDENTIFICADOR:
                registro = self.tabla_símbolos.verificar_existencia(nodo.contenido)
            nodo.visitar(self)

        # Anoto el tipo de retorno (TIPO)
        if nodo_actual.contenido.texto == 'Muestre' or nodo_actual.contenido.texto == 'Mimir':
            nodo_actual.atributos['tipo'] = TipoDatos.NINGUNO

        elif nodo_actual.contenido.texto == 'Ingrese' or nodo_actual.contenido.texto == 'Mezclar_ingredientes':
            nodo_actual.atributos['tipo'] = TipoDatos.TEXTO

        else:
            nodo_actual.atributos['tipo'] = TipoDatos.NINGUNO


    def __visitar_programa(self, nodo_actual:NodoÁrbol):
        """
        --Programa ::= (Comentario | Asignación | Función)* Principal
        ++
        """
        for nodo in nodo_actual.nodos:
            # acá 'self' quiere decir que al método 'visitar' le paso el
            # objetto visitante que estoy usando (o sea, este mismo...
            # self)
            if self.tabla_símbolos.recuperandose_error:
                self.tabla_símbolos.recuperandose_error = False
            nodo.visitar(self)

    def __visitar_asignación(self, nodo_actual:NodoÁrbol):
        """
        --Asignación ::= Identificador metale (Identificador | Literal | ExpresiónMatemática | Invocación )
        ++Asignación ::= ponga Identificador = ( Identificador | Literal | Invocación | ExpresiónMatemática )
        """
        # Metó la información en la tabla de símbolos (IDENTIFICACIÓN)
        self.tabla_símbolos.nuevo_registro(nodo_actual.nodos[0])

        for nodo in nodo_actual.nodos:
            # Verifico que exista si es un identificador (IDENTIFICACIÓN)
            if nodo.tipo == TipoNodo.IDENTIFICADOR:
                registro = self.tabla_símbolos.verificar_existencia(nodo.contenido)
            nodo.visitar(self)

        # Si es una función verifico el tipo que retorna para incluirlo en
        # la asignación y si es un literal puedo anotar el tipo (TIPO) 

        nodo_actual.atributos['tipo'] = nodo_actual.nodos[1].atributos['tipo']

        nodo_actual.nodos[0].atributos['tipo'] = nodo_actual.nodos[1].atributos['tipo']


    def __visitar_expresión_matemática(self, nodo_actual:NodoÁrbol):
        """
        --ExpresiónMatemática ::= (Expresión) | Número | Identificador
        ++ExpresiónMatematica ::= (Expresion) | Literal | Identificador ()*

        Ojo esto soportaría un texto
        """
        for nodo in nodo_actual.nodos:

            # Verifico que exista si es un identificador (IDENTIFICACIÓN)
            if nodo.tipo == TipoNodo.IDENTIFICADOR:
                registro = self.tabla_símbolos.verificar_existencia(nodo.contenido)

            nodo.visitar(self)

        # Anoto el tipo de datos 'NÚMERO' (TIPO)
        nodo_actual.atributos['tipo'] = TipoDatos.NÚMERO

    """
    def __visitar_operador_lógico(self, nodo_actual:NodoÁrbol):
        print(':D code me')
    """

    def __visitar_expresión(self, nodo_actual:NodoÁrbol):
        """
        --Expresión ::= ExpresiónMatemática Operador ExpresiónMatemática
        ++Expresión ::= ExpresiónMatemática Operador ExpresiónMatemática (Operador ExpresiónMatemática)*

        """
        for nodo in nodo_actual.nodos:
            nodo.visitar(self)

        # Anoto el tipo de datos 'NÚMERO' (TIPO)
        nodo_actual.atributos['tipo'] = TipoDatos.NÚMERO

    def __visitar_función(self, nodo_actual:NodoÁrbol):
        """
        --Función ::= (Comentario)? mae Identificador (ParámetrosFunción) BloqueInstrucciones
        ++Función ::= inicio Identificador(Parámetros*)
                            Instrucción+ 
                       fin
        """

        # Meto la función en la tabla de símbolos (IDENTIFICACIÓN)
        self.tabla_símbolos.nuevo_registro(nodo_actual)

        self.tabla_símbolos.abrir_bloque()

        for nodo in nodo_actual.nodos:
            #print(nodo)
            nodo.visitar(self)

        self.tabla_símbolos.cerrar_bloque()
        
        # Anoto el tipo de retorno (TIPO)
        nodo_actual.atributos['tipo'] = nodo_actual.nodos[-1].atributos['tipo']


    def __visitar_invocación(self, nodo_actual):
        """
        --Invocación ::= Identificador ( ParámetrosInvocación )
        ++Invocación ::= llamese Identificador (Parámetros)

        """

        # Verfica que el 'Identificador' exista (IDENTIFICACIÓN) y que sea
        registro = self.tabla_símbolos.verificar_existencia(nodo_actual.nodos[0].contenido)

        if registro['referencia'].tipo != TipoNodo.FUNCION:
            self.manejar_errores(registro['referencia'],TipoNodo.INVOCACION)
            #raise Exception('Esa vara es una variable...', registro)

        for nodo in nodo_actual.nodos:
            nodo.visitar(self)

        # El tipo resultado de la invocación es el tipo inferido de una
        # función previamente definida
        nodo_actual.atributos['tipo'] = registro['referencia'].atributos['tipo']


    def __visitar_parámetros_invocación(self, nodo_actual):
        """
        --ParámetrosInvocación ::= Valor (/ Valor)+
        ++Parametros_invocacion ::= Valor (, Valor )*

        """

        # Recordemos que 'Valor' no existe en el árbol...

        # Si es 'Identificador' verifico que exista (IDENTIFICACIÓN)
        for nodo in nodo_actual.nodos:
            # Si existe y no es función ya viene con el tipo por que
            # fue producto de una asignación
            if nodo.tipo == TipoNodo.IDENTIFICADOR:
                registro = self.tabla_símbolos.verificar_existencia(nodo.contenido)

            elif nodo.tipo == TipoNodo.FUNCION:
                self.manejar_errores(registro['referencia'],TipoNodo.PARAMETROS_INVOCACION)
                #raise Exception('Esa vara es una función...', nodo.contenido) 

            # Si es número o texto nada más los visito
            nodo.visitar(self)

        # No hay tipos en los parámetros... se sabe en tiempo de ejecución


    def __visitar_parámetros_función(self, nodo_actual):
        """
        --ParámetrosFunción ::= Identificador (/ Identificador)+
        ++Parametros_funcion ::= Identificador (, Identificador )*

        """
        #print(nodo_actual)
        # Registro cada 'Identificador' en la tabla
        for nodo in nodo_actual.nodos:
            #print(nodo)
            #print(nodo.contenido.texto) 
            self.tabla_símbolos.nuevo_registro(nodo)
            nodo.visitar(self)


    def __visitar_instrucción(self, nodo_actual):
        """
        --Instrucción ::= (Repetición | Bifurcación | (Asignación | Invocación) | Retorno | Error | Comentario )
        ++Instrucción ::= (Lácteo | Asignación | Bifurcación | Retorno | Comentario | Repetición)    
        """
        # Por alguna razón no me volé este nivel.. así que lo visitamos... 
        # Esto es un desperdicio de memoria y de cpu

        # Visita la instrucción 

        # Lo pongo así por copy/paste... pero puede ser como el comentario
        # de más abajo.
        for nodo in nodo_actual.nodos:
            nodo.visitar(self)
            nodo_actual.atributos['tipo'] = nodo.atributos['tipo']

        # nodo_actual.nodos[0].visitar(self)

    def __visitar_repetición(self, nodo_actual):
        """
        --Repetición ::= upee ( Condición ) BloqueInstrucciones
        ++Repetición ::= sea_necio (Condición) #Instrucción+#

        """
        # Visita la condición


        # Visita el bloque de instrucciones

        # Lo pongo así por copy/paste... pero puede ser como el comentario
        # de más abajo.
        self.tabla_símbolos.abrir_bloque()

        for nodo in nodo_actual.nodos:
            nodo.visitar(self)

        # nodo_actual.nodos[0].visitar(self)

        self.tabla_símbolos.cerrar_bloque()

        # Anoto el tipo de retorno (TIPO)
        nodo_actual.atributos['tipo'] = nodo_actual.nodos[1].atributos['tipo']


    def __visitar_bifurcación(self, nodo_actual):
        """
        --Bifurcación ::= DiaySi (Sino)?
        ++Bifurcación ::= SiSucede (Sino)?

        """

        # Visita los dos nodos en el siguiente nivel si los hay
        for nodo in nodo_actual.nodos:
            nodo.visitar(self)

        nodo_actual.atributos['tipo'] = TipoDatos.CUALQUIERA 

    def __visitar_sisucede(self, nodo_actual):
        """
        --DiaySi ::= diay siii ( Condición ) BloqueInstrucciones
        ++SiSucede ::= si_pasa (Condición) #Instrucción+#
        """


        # Visita la condición


        # Visita el bloque de instrucciones

        # Lo pongo así por copy/paste... pero puede ser como el comentario
        # de más abajo.
        self.tabla_símbolos.abrir_bloque()

        for nodo in nodo_actual.nodos:
            nodo.visitar(self)

        # nodo_actual.nodos[0].visitar(self)

        self.tabla_símbolos.cerrar_bloque()

        # Anoto el tipo de retorno (TIPO)
        nodo_actual.atributos['tipo'] = nodo_actual.nodos[1].atributos['tipo']

    def __visitar_sino(self, nodo_actual):
        """
        --Sino ::= sino ni modo BloqueInstrucciones
        ++Sino ::= tal_vez #Instrucción+#

        """
        # Visita el bloque de instrucciones

        # Lo pongo así por copy/paste... pero puede ser como el comentario
        # de más abajo.
        self.tabla_símbolos.abrir_bloque()

        for nodo in nodo_actual.nodos:
            nodo.visitar(self)

        # nodo_actual.nodos[0].visitar(self)

        self.tabla_símbolos.cerrar_bloque()

        # Anoto el tipo de retorno (TIPO)
        nodo_actual.atributos['tipo'] = nodo_actual.nodos[0].atributos['tipo']

    def __visitar_condición(self, nodo_actual):
        """
        --Condición ::= Comparación ((divorcio|casorio) Comparación)?
        ++Condición ::= Comparación ((agregue|intente_esta) Comparación )?

        """

        for nodo in nodo_actual.nodos:
            nodo.visitar(self)

        # Comparación retorna un valor de verdad (TIPO)
        nodo_actual.atributos['tipo'] = TipoDatos.VALOR_VERDAD


    def __visitar_comparación(self, nodo_actual):
        """
        --Comparación ::= Valor Comparador Valor
        ++Comparación ::= Valor Comparador Valor

        """

        # Si los 'Valor' son identificadores se asegura que existan (IDENTIFICACIÓN)
        for nodo in nodo_actual.nodos:
            if nodo.tipo == TipoNodo.IDENTIFICADOR:
                registro = self.tabla_símbolos.verificar_existencia(nodo.contenido)

            nodo.visitar(self)


        # Verifico que los tipos coincidan (TIPO)
        valor_izq      = nodo_actual.nodos[0]
        comparador  = nodo_actual.nodos[1]
        valor_der      = nodo_actual.nodos[2]
        # Ya se que eso se ve sueltelefeo... pero ya el cerebro se me apagó...

        if valor_izq.atributos['tipo'] == valor_der.atributos['tipo']:
            comparador.atributos['tipo'] = valor_izq.atributos['tipo']

            # Una comparación siempre tiene un valor de verdad
            nodo_actual.atributos['tipo'] = TipoDatos.VALOR_VERDAD

        # Caso especial loco: Si alguno de los dos es un identificador de
        # un parámetro de función no puedo saber que tipo tiene o va a
        # tener por que este lenguaje no es tipado... tons vamos a poner
        # que la comparación puede ser cualquiera
        elif valor_izq.atributos['tipo'] == TipoDatos.CUALQUIERA or \
                valor_der.atributos['tipo'] == TipoDatos.CUALQUIERA:

            comparador.atributos['tipo'] = TipoDatos.CUALQUIERA

            # Todavía no estoy seguro.
            nodo_actual.atributos['tipo'] = TipoDatos.CUALQUIERA

        else:
            componente = nodo_actual.contenido
            texto = '<Error: El valor '+componente.texto+' en la fila '+str(componente.atributos_adicionales.fila)+', en la columna ' +str(componente.atributos_adicionales.columna)+' hubo un error en la comparación'
            self.tabla_símbolos.lista_errores.append(texto)
            self.tabla_símbolos.recuperandose_error = True
            #raise Exception('Papo, algo tronó acá', str(nodo_actual))

    def __visitar_valor(self, nodo_actual):
        """
        --Valor ::= (Identificador | Literal)
        ++Valor ::= (Identificador | Literal)

        """
        # En realidad núnca se va a visitar por que lo saqué del árbol
        # duránte la etapa de análisiss

    def __visitar_retorno(self, nodo_actual):
        """
        --Retorno :: sarpe (Valor)?
        ++Retorno ::= monchar Valor?
        """

        for nodo in nodo_actual.nodos:
            nodo.visitar(self)
        
        if nodo_actual.nodos == []:
            # Si no retorna un valor no retorna un tipo específico 
            nodo_actual.atributos['tipo'] = TipoDatos.NINGUNO

        else:

            for nodo in nodo_actual.nodos:

                nodo.visitar(self)

                if nodo.tipo == TipoNodo.IDENTIFICADOR:
                    # Verifico si valor es un identificador que exista (IDENTIFICACIÓN)
                    registro = self.tabla_símbolos.verificar_existencia(nodo.contenido)
                    if registro != None: 
                        # le doy al sarpe el tipo de retorno del identificador encontrado
                        nodo_actual.atributos['tipo'] = registro['referencia'].atributos['tipo']

                else:
                    # Verifico si es un Literal de que tipo es (TIPO)
                    nodo_actual.atributos['tipo'] = nodo.atributos['tipo']

    def __visitar_principal(self, nodo_actual):
        """
        --Principal ::= (Comentario)?  (jefe | jefa) mae BloqueInstrucciones
        ++Principal ::= inicio_Receta Invocación

        """
        # Este mae solo va a tener un bloque de instrucciones que tengo que
        # ir a visitar

        # Lo pongo así por copy/paste... pero puede ser como el comentario
        # de más abajo.
        for nodo in nodo_actual.nodos:
            nodo.visitar(self)

        # nodo_actual.nodos[0].visitar(self)

        # Anoto el tipo de retorno (TIPO)
        nodo_actual.atributos['tipo'] = nodo_actual.nodos[0].atributos['tipo']


    def __visitar_bloque_instrucciones(self, nodo_actual):
        """
        --BloqueInstrucciones ::= { Instrucción+ }
        ++BloqueInstrucciones ::= { Instrucción+ }

        """
        # Visita todas las instrucciones que contiene
        for nodo in nodo_actual.nodos:
            nodo.visitar(self)

        # Acá yo debería agarrar el tipo de datos del Retorno si lo hay
        nodo_actual.atributos['tipo'] = TipoDatos.NINGUNO 

        for nodo in nodo_actual.nodos:
            if nodo.atributos['tipo'] != TipoDatos.NINGUNO:
                nodo_actual.atributos['tipo'] = nodo.atributos['tipo']

    def __visitar_operador(self, nodo_actual):
        """
        --Operador ::= (hechele | quitele | chuncherequee | desmadeje)
        ++Operador ::= ( + | - | * | /)
        """
        # Operador para trabajar con números (TIPO)
        nodo_actual.atributos['tipo'] = TipoDatos.NÚMERO

    def __visitar_veracidad(self, nodo_actual):
        """
        --ValorVerdad ::= (True | False)
        ++Veracidad ::= (alchile | noinvente)

        """
        # Valor de verdad (TIPO)
        nodo_actual.atributos['tipo'] = TipoDatos.VALOR_VERDAD

    def __visitar_comparador(self, nodo_actual):
        """
        --Comparador ::= (cañazo | poquitico | misma vara | otra vara | menos o igualitico | más o igualitico)
        ++Comparador ::= (igual | diferente | menor | mayor | menor_igual | mayor_igual)
        """
        # Estos comparadores son numéricos  (TIPO) 
        # (cañazo | poquitico | misma vara | otra vara | menos o igualitico | más o igualitico)
        if nodo_actual.contenido not in ['misma vara', 'otra vara' ]:
            nodo_actual.atributos['tipo'] = TipoDatos.NÚMERO

        else:
            nodo_actual.atributos['tipo'] = TipoDatos.CUALQUIERA
            # Si no es alguno de esos puede ser Numérico o texto y no lo puedo
            # inferir todavía


    def __visitar_texto(self, nodo_actual):
        """
        Texto ::= ~/\w(\s\w)*)?~
        """
        # Texto (TIPO)
        nodo_actual.atributos['tipo'] = TipoDatos.TEXTO

    def __visitar_entero(self, nodo_actual):
        """
        Entero ::= (-)?\d+
        """
        # Entero (TIPO) 
        nodo_actual.atributos['tipo'] = TipoDatos.NÚMERO

    def __visitar_flotante(self, nodo_actual):
        """
        Flotante ::= (-)?\d+.(-)?\d+
        """
        # Flotante (TIPO) 
        nodo_actual.atributos['tipo'] = TipoDatos.NÚMERO

    def __visitar_identificador(self, nodo_actual):
        """
        --Identificador ::= [a-z][a-zA-Z0-9]+
        ++Identificador ::= [a-zA-Z]([a-zA-Z0-9_])*

        """
        nodo_actual.atributos['tipo'] = TipoDatos.CUALQUIERA
        # No hace nada

