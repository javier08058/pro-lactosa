import sys
import time

def Mezclar_ingredientes(texto1, texto2):
    return texto1 + texto2

def Mimir(tiempo):
    try:
        time.sleep(float(tiempo))
    except:
        raise Exception("La variable tiempo no es un numerico")

def Muestre(texto):
    print(texto)

def Ingrese():
    dato = input()
    if dato.isnumeric():
        return int(dato)
    return dato


def getTiempoCocina(numero,aleatorio):
    tiempo = aleatorio / 30 * numero
    resulta = tiempo * 100
    return resulta

def paso1(Pan,tiempoPaso1):
    tiempoPaso1_ = 2
    Muestre(str("Paso 1 "))
    Muestre(str("seleccionando el pan preferido el cual es ")+str(Pan))
    Mimir(str(tiempoPaso1))

def paso2(Pan,tiempoPaso2):
    Muestre(str("Paso 2"))
    Muestre(str("Ingresando el pan seleccionado ")+str(Pan)+str(" a la tostadora"))
    Mimir(str(tiempoPaso2))

def paso3(Lechuga,Tomate,Pepinillos,Cebolla,Jalapenos,Aceitunas,tiempoPaso3):
    Muestre(str("Paso 3"))
    Muestre(str("Agregando los siguientes ingredientes al pan")+str(Lechuga)+str(",")+str(Tomate)+str(",")+str(Cebolla)+str(",")+str(Jalapenos)+str(",")+str(Aceitunas))       
    Mimir(str(tiempoPaso3))

def paso4(Salsa,tiempoPaso4):
    Muestre(str("Paso 4"))
    Muestre(str("Agregando la salsa preferida")+str(Salsa))
    Mimir(str(tiempoPaso4))

def Principal():
    Muestre(str("\n ***************** RECETA DE SANDWICH VEGETARIANO *****************"))
    I1 = "pepinos"
    I2 = "pimientosverdes"
    I3 = "lechugas"
    I4 = "cebolla morada"
    I5 = "espinacas"
    I6 = "tomates"
    I7 = "chiles banana"
    I8 = "jalapeÃ±os"
    I9 = "aceitunas negras"
    I10 = "pepinillos"
    I11 = "Pan con orÃ©gano y parmesano"
    I12 = "Chipotle Southwest"
    I13 = "mayonesa light"
    Muestre(str("Ingredientes disponibles en el menu:"))
    Muestre(str("   -")+str(I1))
    Muestre(str("   -")+str(I2))
    Muestre(str("   -")+str(I3))
    Muestre(str("   -")+str(I4))
    Muestre(str("   -")+str(I5))
    Muestre(str("   -")+str(I6))
    Muestre(str("   -")+str(I7))
    Muestre(str("   -")+str(I8))
    Muestre(str("   -")+str(I9))
    Muestre(str("   -")+str(I10))
    Muestre(str("   -")+str(I11))
    Muestre(str("   -")+str(I12))
    Muestre(str("   -")+str(I13))
    jugadores = 1
    tiempoP1 = 0
    tiempoP2 = 0
    while jugadores < 3  :
        Muestre(str("\n>>>>>>> Turno del jugador # ")+str(jugadores)+str(" <<<<<<<"))
        Muestre(str("\nPor favor ingrese el su nivel de maestria en cocina \n"))
        Muestre(str("-Avanzado: 1"))
        Muestre(str("-Intermedio: 2"))
        Muestre(str("-Principiante: 3"))
        nivel = ""
        while nivel == ""  :
            Muestre(str("Inserte el nivel"))
            nivel = Ingrese()
        Muestre(str("\nPor favor ingrese un numero aleatorio del 1 al 10:"))
        aleatorio = ""
        while aleatorio == ""  :
            Muestre(str("Inserte el numero aleatorio"))
            aleatorio = Ingrese()
        Muestre(str("Proceso del jugador:"))
        espera = getTiempoCocina(nivel,aleatorio)
        if jugadores == 1  :
            tiempoP1 = espera
        if jugadores == 2  :
            tiempoP2 = espera
        nuevaEspera = espera / 4
        paso1(I11,nuevaEspera)
        paso2(I11,nuevaEspera)
        paso3(I3,I6,I10,I4,I8,I9,nuevaEspera)
        paso4(I12,nuevaEspera)
        jugadores = jugadores + 1
    if tiempoP1 > tiempoP2  :
        Muestre(str("El jugador 2 es el ganador con un tiempo de : ")+str(tiempoP2)+str(" segundos"))
    if tiempoP2 > tiempoP1  :
        Muestre(str("El jugador 1 es el ganador con un tiempo de : ")+str(tiempoP1)+str(" segundos"))
    if tiempoP1 == tiempoP2  :
        Muestre(str("Ambos jugadores obtuvieron el mismo tiempo durante la prueba de cocina quedan empatados"))

if __name__ == '__main__':
    Principal()