#Analizador de la gramática (sin la creación del árbol) del lenguaje prolactosa

from os import error
from explorador.TipoComponente import TipoComponente
from utils.árbol import ÁrbolSintáxisAbstracta, NodoÁrbol, TipoNodo

class Analizador:
    def __init__(self, lista_componentes):
        self.errores = []
        self.recupendoseError = False
        self.siguientInicioIndice = None
        self.componentes_léxicos = lista_componentes
        self.cantidad_componentes = len(lista_componentes)
        self.instruccionAnalizadaActual = None
        self.posición_componente_actual = 0
        self.asa = ÁrbolSintáxisAbstracta()

        self.componente_actual = lista_componentes[0]

    def tiene_errores(self):
        ''' Funcion para determinar si el analizador detectó errores
            en los componentes.
        '''
        if len(self.errores) > 0:
            return True
        return False

    def imprimir_errores(self):
        ''' Funcion para imprimir los errores que detectó el analizador
        '''
        print('\n','Lista de Errores Analizador:')
        for error in self.errores:
            print('\n','Error #'+str(self.errores.index(error)+1))
            print(error)
        print('\n','Fin de lista de Errores')
        
    def imprimir_asa(self):
        '''imprime el arbol de sintaxis abstracta'''
        if self.asa.raiz is None :
            print([])
        else:
            self.asa.imprimir_preorden()

    def analizar(self):
        """
        Método principal que inicia el análisis siguiendo el esquema de
        análisis por descenso recursivo
        """
        self.asa.raiz = self.__analizar_programa()

    def manejar_errores(self):
        """
        Método que se encarga de manejar el reporte de errores
        """
        if self.recupendoseError == False:
            self.recupendoseError = True
            
            
            instruccion = self.instruccionAnalizadaActual
            texto       = self.componente_actual.texto
            tipo        = self.componente_actual.tipo.name
            fila        = self.componente_actual.atributos_adicionales.fila
            columna     = self.componente_actual.atributos_adicionales.columna
            #####
            error = f'>> Error en la instruccion: {instruccion} \nTexto: {texto} \nTipo: {tipo}  \nLinea: {fila} \nColumna: {columna}'
            self.errores.append(error)
            ####
            self.__buscar_siguiente_funcion()

    def __buscar_siguiente_funcion(self):
        for i in range(len(self.componentes_léxicos)):
            if i > self.posición_componente_actual \
                and self.componentes_léxicos[i].texto == 'inicio':
                self.siguientInicioIndice = i
                return

    def __analizar_programa(self):
        """
        Programa ::= (Comentario | Asignación | Función)* Principal
        """

        """
        Los comentarios se ignoran desde el explorador, entonces no se tratan en el analizador
        """
        nodos_nuevos=[]
        self.instruccionAnalizadaActual = 'Programa'

        #Pueden venir varias asignaciones o funciones nuevas
        while(True):
            if self.recupendoseError:
                if self.siguientInicioIndice == None:
                    return
                self.componente_actual = \
                    self.componentes_léxicos[self.siguientInicioIndice]
                self.posición_componente_actual = self.siguientInicioIndice
                self.recupendoseError = False
                self.siguientInicioIndice = None
                
                
            # Si es asignación
            elif (self.componente_actual.tipo == TipoComponente.PALABRA_CLAVE) and (self.componente_actual.texto == 'ponga'):
                nodos_nuevos+=[self.__analizar_asignación()]

            # Si es función
            elif (self.componente_actual.tipo == TipoComponente.PALABRA_CLAVE) and (self.componente_actual.texto == 'inicio'):
                nodos_nuevos+=[self.__analizar_función()]
            
            else:
                break

        # Después de salir del while, tiene que entrar siempre al Pirncipal
        if (self.componente_actual.tipo == TipoComponente.PALABRA_CLAVE) and (self.componente_actual.texto == 'inicio_Receta'):
            nodos_nuevos+=[self.__analizar_principal()]
        else:
            self.manejar_errores()
        return NodoÁrbol(TipoNodo.PROGRAMA,nodos=nodos_nuevos)


    def __analizar_asignación(self):
        """
        Asignación ::= ponga Identificador = ( Literal | Invocación | ExpresiónMatemática)
        """
        nodos_nuevos=[]
        self.instruccionAnalizadaActual = 'Asignación'

        # Se verifica que el componente corresponda a "ponga"
        self.__verificar('ponga')

        # El identificador en esta posición es obligatorio
        nodos_nuevos+=[self.__verificar_identificador()]

        # Se verifica que el componente corresponda a "="
        self.__verificar('=')

        # El componenente puede ser Un Literal, expresión matemática o invocación
        if self.componente_actual.tipo in [TipoComponente.ENTERO, TipoComponente.FLOTANTE, TipoComponente.VALOR_VERDAD, TipoComponente.TEXTO] :
            nodos_nuevos+=[self.__analizar_literal()]

        # En expresion matemática, tiene que ir entre parentesis
        elif self.componente_actual.texto == '(': 
            nodos_nuevos+=[self.__analizar_expresión_matemática()]

        # Si es un ambiente estandar
        elif self.componente_actual.texto in ['Ingrese', 'Mezclar_Ingredientes']:
            nodos_nuevos+=[self.__analizar_ambiente_estandar()]
        
        # Si es un identificador
        elif self.componente_actual.tipo == TipoComponente.IDENTIFICADOR:
            nodos_nuevos+=[self.__verificar_identificador()]

        # si no es ninguna de las otras, es invocación
        else:
            nodos_nuevos+=[self.__analizar_invocación()]
            
        return NodoÁrbol(TipoNodo.ASIGNACION,nodos=nodos_nuevos)


    
    def __analizar_expresión_matemática(self):
        """
        ExpresiónMatematica ::= (Expresion) | Literal | Identificador ()*
        """
        nodos_nuevos=[]
        self.instruccionAnalizadaActual = "Expresión Matematica"
        
        # Si es una expresion
        if self.componente_actual.texto == '(':
            self.__verificar('(')
            nodos_nuevos+=[self.__analizar_expresión()]
            self.__verificar(')')

        # Si no es, verifica que sea literal y lo analiza
        elif self.componente_actual.tipo in [TipoComponente.ENTERO, TipoComponente.FLOTANTE, TipoComponente.VALOR_VERDAD, TipoComponente.TEXTO] :
            nodos_nuevos+=[self.__analizar_literal()]

        # Si no es ninguno de los anteriores es esta
        else:
            nodos_nuevos+=[self.__verificar_identificador()]
        return NodoÁrbol(TipoNodo.EXPRESIÓN_MATEMÁTICA,nodos=nodos_nuevos)

    
    def __analizar_expresión(self):
        """
        Expresión ::= ExpresiónMatemática Operador ExpresiónMatemática (Operador ExpresiónMatemática)*
        (C1/5)
        """
        nodos_nuevos=[]
        self.instruccionAnalizadaActual = 'Expresión'
        #Todos los componentes son fijos por lo que deben de venir así
        nodos_nuevos+=[self.__analizar_expresión_matemática()]
        nodos_nuevos+=[self.__verificar_operador()]
        nodos_nuevos+=[self.__analizar_expresión_matemática()]
        while self.componente_actual.texto!=')':
            if self.componente_actual.tipo==TipoComponente.OPERADOR:
                nodos_nuevos+=[self.__verificar_operador()]
                nodos_nuevos+=[self.__analizar_expresión_matemática()]
            else:
                self.manejar_errores()
                break
        return NodoÁrbol(TipoNodo.EXPRESIÓN,nodos=nodos_nuevos)

    def __analizar_función(self):
        """
        Función ::= inicio Identificador(Parámetros*)  
                            Instrucción+ 
       	            fin
        """
        nodos_nuevos=[]
        self.instruccionAnalizadaActual = 'Función'
        # Esta sección es obligatoria en este orden
        self.__verificar('inicio')
        nodos_nuevos+=[self.__verificar_identificador()]
        self.__verificar('(')
        if self.componente_actual.texto != ')':
            nodos_nuevos+=[self.__analizar_parámetros_funcion()]
        self.__verificar(')')
        nodos_nuevos+=[self.__analizar_bloque_instrucciones()]
        self.__verificar('fin')
        return NodoÁrbol(TipoNodo.FUNCION,contenido=nodos_nuevos[0].contenido,nodos=nodos_nuevos)


    def __analizar_invocación(self):
        """
        Invocación ::= llamese Identificador (Parámetros)
        """
        nodos_nuevos = []
        self.instruccionAnalizadaActual = 'Invocación'

        # Esta sección es obligatoria en este orden
        self.__verificar('llamese')
        nodos_nuevos+= [self.__verificar_identificador()]
        self.__verificar('(')
        if self.componente_actual.texto != ')':
            nodos_nuevos+= [self.__analizar_parámetros_invocacion()]
        self.__verificar(')')
        return NodoÁrbol(TipoNodo.INVOCACION,nodos=nodos_nuevos)

    def __analizar_parámetros_funcion(self):
        '''        
        Parametros_funcion ::= Identificador (, Identificador )*
        '''
        nodos_nuevos=[]
        self.instruccionAnalizadaActual = 'Parametros funcion'

        # Siempre debe de haber al menos un Valor
        nodos_nuevos+=[self.__verificar_identificador()]
        while( self.componente_actual.texto == ','):
            self.__verificar(',')
            nodos_nuevos+=[self.__verificar_identificador()]
        return NodoÁrbol(TipoNodo.PARAMETROS_FUNCION,nodos=nodos_nuevos)
        
    def __analizar_parámetros_invocacion(self):
        '''        
        Parametros_invocacion ::= Valor (, Valor )*
        '''
        nodos_nuevos=[]
        self.instruccionAnalizadaActual = 'Parametros invocacion'

        # Siempre debe de haber al menos un Valor
        nodos_nuevos+=[self.__analizar_valor()]
        while( self.componente_actual.texto == ','):
            self.__verificar(',')
            nodos_nuevos+=[self.__analizar_valor()]
        return NodoÁrbol(TipoNodo.PARAMETROS_INVOCACION,nodos=nodos_nuevos)

    def __analizar_instrucción(self):
        """
        Instrucción ::= (Lácteo | Asignación | Bifurcación | Retorno | Comentario | Repetición)
        """
        nodos_nuevos=[]
        self.instruccionAnalizadaActual = 'Instrucción'

        #Se busca cual tipo de componente es la instrucción
        #Como Lácteo ya se lanza como error en el explorador, ahora se ignora
        if self.componente_actual.texto == 'ponga':
            nodos_nuevos+=[self.__analizar_asignación()]

        #Se revisa si llena el "if" de las bifurcaciones
        elif self.componente_actual.texto == 'si_pasa':
            nodos_nuevos+=[self.__analizar_bifurcación()]

        #Se revisa si lleva el "return" del lenguaje
        elif self.componente_actual.texto == 'monchar':
            nodos_nuevos+=[self.__analizar_retorno()]

        #Se revisa si lleva el "while" del lenguaje
        elif self.componente_actual.texto == 'sea_necio':
            nodos_nuevos+=[self.__analizar_repetición()]
        
        #Se revisa si lleva es una funcion de ambiente estandar del lenguaje
        elif self.componente_actual.texto in ['Muestre','Ingrese','Mezclar_ingredientes','Mimir']:
            nodos_nuevos+=[self.__analizar_ambiente_estandar()]
        
        # Se revisa si lleva es una invocacion
        elif self.componente_actual.texto == 'llamese':
            nodos_nuevos+=[self.__analizar_invocación()]
        #Se ignoran los comentarios
        #Si no es ninguno de los anteriores, debe de ser un error
        else:
            self.manejar_errores()
        return NodoÁrbol(TipoNodo.INSTRUCCION,nodos=nodos_nuevos)


    def __analizar_ambiente_estandar(self):
        """
        Muestre (Texto) : Imprime el texto en la pantalla
        Ingrese(Texto) : Pide al usuario que ingrese un texto
        Mezclar_ingredientes(Texto1, Texto2): Junta dos textos y devuelve uno
        Mimir(Número) : Espera X tiempo para continuar
        """
        nodos_nuevos=[]
        self.instruccionAnalizadaActual = 'Ambiente Estandar'
        nodo = NodoÁrbol(TipoNodo.AMBIENTE_ESTANDAR, contenido=self.componente_actual)
        nodos_nuevos+=[nodo]
        # Esta sección es obligatoria en este orden
        if self.componente_actual.texto ==  'Muestre' or self.componente_actual.texto ==  'Ingrese':
            self.__verificar_tipo_componente(TipoComponente.AMBIENTEESTANDAR)
            self.__verificar('(')
            while self.componente_actual.tipo in [TipoComponente.TEXTO, TipoComponente.OPERADOR, TipoComponente.IDENTIFICADOR]\
                and self.componente_actual.texto != ')' and self.recupendoseError == False:
                if self.componente_actual.tipo is TipoComponente.TEXTO:
                    nodos_nuevos+=[self.__verificar_texto()]
                elif self.componente_actual.texto == '+':
                    nodos_nuevos+=[self.__verificar_operador()]
                else:
                    nodos_nuevos+=[self.__verificar_identificador()]
            self.__verificar(')')

        elif self.componente_actual.texto ==  'Mezclar_ingredientes':
            self.__verificar_tipo_componente(TipoComponente.AMBIENTEESTANDAR)
            self.__verificar('(')
            nodos_nuevos+=[self.__analizar_parámetros_invocacion()]
            self.__verificar(')')

        elif self.componente_actual.texto == 'Mimir':
            self.__verificar_tipo_componente(TipoComponente.AMBIENTEESTANDAR)
            self.__verificar('(')
            nodos_nuevos+=[self.__verificar_identificador()]
            self.__verificar(')')    
        else:
            self.manejar_errores()    
        return NodoÁrbol(TipoNodo.AMBIENTE_ESTANDAR,contenido=nodos_nuevos[0].contenido,nodos=nodos_nuevos)
       

    def __analizar_repetición(self):
        """
        Repetición ::= sea_necio (Condición) #Instrucción+#
        """
        nodos_nuevos=[]
        self.instruccionAnalizadaActual = 'Repetición'

        # Esta sección es obligatoria en este orden
        self.__verificar('sea_necio')
        self.__verificar('(')
        nodos_nuevos+=[self.__analizar_condición()]
        self.__verificar(')')
        self.__verificar('#')
        nodos_nuevos+=[self.__analizar_bloque_instrucciones()]
        self.__verificar('#')
        return NodoÁrbol(TipoNodo.REPETICION,nodos=nodos_nuevos)




    def __analizar_bifurcación(self):
        """
        Bifurcación ::= SiSucede (Sino)?
        """
        nodos_nuevos=[]
        self.instruccionAnalizadaActual = 'Bifurcación'

        # Se analiza el Sisucede porque es obligatorio
        nodos_nuevos+=[self.__analizar_sisucede()]

        # el Sino es opcional, se revisa si está presente o no
        if self.componente_actual.texto == 'tal_vez':
            nodos_nuevos+=[self.__analizar_sino()]
        return NodoÁrbol(TipoNodo.BIFURCACION,nodos=nodos_nuevos)


    def __analizar_sisucede(self):
        """
        SiSucede ::= si_pasa (Condición) #Instrucción+#
        """
        nodos_nuevos=[]
        self.instruccionAnalizadaActual = 'SiSucede'

        # Esta sección es obligatoria en este orden
        self.__verificar('si_pasa')
        self.__verificar('(')
        nodos_nuevos+=[self.__analizar_condición()]
        self.__verificar(')')
        self.__verificar('#')
        nodos_nuevos+=[self.__analizar_bloque_instrucciones()]
        self.__verificar('#')
        return NodoÁrbol(TipoNodo.SISUCEDE,nodos=nodos_nuevos)


    def __analizar_sino(self):
        """
        Sino ::= tal_vez #Instrucción+#
        """
        nodos_nuevos=[]
        self.instruccionAnalizadaActual = 'Sino'

        # Esta sección es obligatoria en este orden
        self.__verificar('tal_vez')
        self.__verificar('#')
        nodos_nuevos+=[self.__analizar_bloque_instrucciones()]
        self.__verificar('#')
        return NodoÁrbol(TipoNodo.SINO,nodos=nodos_nuevos)


    def __analizar_condición(self):
        """
        Condición ::= Comparación ((agregue|intente_esta) Comparación )?
        """
        nodos_nuevos=[]
        self.instruccionAnalizadaActual = 'Condición'

        # La condición necesita obligatoriamente una comparación
        nodos_nuevos+=[self.__analizar_comparación()]

        # Esta sección es opcional, puede venir con más comparaciones
        if self.componente_actual.tipo == TipoComponente.PALABRA_CLAVE:
            nodo=NodoÁrbol(TipoNodo.OPERADOR_LOGICO,contenido=self.componente_actual)
            nodos_nuevos+=[nodo]
            # aguegue es el equivalente a AND
            if self.componente_actual.texto == 'agregue':
                self.__verificar('agregue')
            
            # intente_esta es el equivalente a OR
            else:
                self.__verificar('intente_esta')
            nodos_nuevos+=[self.__analizar_comparación()]
        return NodoÁrbol(TipoNodo.CONDICION,nodos=nodos_nuevos)


    def __analizar_comparación(self):
        """
        Comparación ::= Valor Comparador Valor
        """
        nodos_nuevos=[]
        self.instruccionAnalizadaActual = 'Comparación'

        # Esta sección es obligatoria en este orden
        nodos_nuevos+=[self.__analizar_valor()]
        nodos_nuevos+=[self.__verificar_comparador()]
        nodos_nuevos+=[self.__analizar_valor()]
        return NodoÁrbol(TipoNodo.COMPARACION,nodos=nodos_nuevos)


    def __analizar_valor(self):
        """
        Valor ::= (Identificador | Literal)
        """
        
        self.instruccionAnalizadaActual = 'Valor'

        # Tiene que ser uno de los dos, se revisa
        if self.componente_actual.tipo is TipoComponente.IDENTIFICADOR:
            nodo = self.__verificar_identificador()
        else:
            nodo = self.__analizar_literal()
        return nodo


    def __analizar_retorno(self):
        """
        Retorno ::= monchar Valor?
        """
        nodos_nuevos = []
        self.instruccionAnalizadaActual = 'Retorno'

        self.__verificar('monchar')

        # Se verifica si es un Valor; si lo es, se analiza, sino, entonces esta vacío
        if self.componente_actual.tipo in [TipoComponente.IDENTIFICADOR, TipoComponente.ENTERO, TipoComponente.FLOTANTE, TipoComponente.VALOR_VERDAD, TipoComponente.TEXTO] :
            nodos_nuevos+=[self.__analizar_valor()]
        return NodoÁrbol(TipoNodo.RETORNO,nodos=nodos_nuevos)


    def __analizar_principal(self):
        """
        Principal ::= inicio_Receta Invocación
        """
        nodos_nuevos = []
        self.instruccionAnalizadaActual = 'Principal'

        # Esta sección es obligatoria en este orden
        self.__verificar('inicio_Receta')
        nodos_nuevos+=[self.__verificar_identificador()]
        self.__verificar('(')
        if self.componente_actual.texto != ')':
            nodos_nuevos+=[self.__analizar_parámetros_funcion()]
        self.__verificar(')')
        return NodoÁrbol(TipoNodo.PRINCIPAL, nodos=nodos_nuevos)
        

    def __analizar_literal(self):
        """
        Literal ::= (Número | Texto | Veracidad)
        """
        self.instruccionAnalizadaActual = 'Literal'

        # Se revisa si el TipoComponente es un texto
        if self.componente_actual.tipo is TipoComponente.TEXTO:
            nodo = self.__verificar_texto()

        # Se revisa si el TipoComponente es una Veracidad
        elif  self.componente_actual.tipo is TipoComponente.VALOR_VERDAD:
            nodo = self.__verificar_valor_verdad()

        # Si no es ninguno de los anteriores, debe de ser un número
        else:
            nodo = self.__analizar_número()
        return nodo


    def __analizar_número(self):
        """
        Número ::= (Entero | Flotante)
        """
        self.instruccionAnalizadaActual = 'Número'
        if self.componente_actual.tipo == TipoComponente.ENTERO:
            nodo= self.__verificar_entero()
        else:
            nodo= self.__verificar_flotante()
        return nodo


    def __analizar_bloque_instrucciones(self):
        """
        BloqueInstrucciones ::= { Instrucción+ }
        """
        nodos_nuevos = []
        self.instruccionAnalizadaActual = 'BloqueInstrucciones'

        if self.recupendoseError == False:
            # Siempre debe de tener mínimo una instrucción
            nodos_nuevos+=[self.__analizar_instrucción()]

            # Después de la obligatorio, puede haber 0 o más instruccciones
            while self.componente_actual.texto in ['ponga', 'si_pasa', 'monchar', 'sea_necio', 'llamese'] or self.componente_actual.tipo == TipoComponente.AMBIENTEESTANDAR:
                nodos_nuevos+=[self.__analizar_instrucción()]
            return NodoÁrbol(TipoNodo.BLOQUE_INSTRUCCIONES,nodos=nodos_nuevos)


    #Componenetes pequeños
    def __verificar_operador(self):
        """
        Operador ::= ( + | - | * | /)
        """
        nodo = NodoÁrbol(TipoNodo.OPERADOR, contenido =self.componente_actual)
        self.__verificar_tipo_componente(TipoComponente.OPERADOR)
        return nodo

    def __verificar_valor_verdad(self):
        """
        Veracidad ::= (alchile | noinvente)
        """
        nodo = NodoÁrbol(TipoNodo.VERACIDAD, contenido =self.componente_actual)
        self.__verificar_tipo_componente(TipoComponente.VALOR_VERDAD)
        return nodo


    def __verificar_comparador(self):
        """
        Comparador ::= (igual | diferente | menor | mayor | menor_igual | mayor_igual)
        """
        nodo = NodoÁrbol(TipoNodo.COMPARADOR, contenido =self.componente_actual)
        self.__verificar_tipo_componente(TipoComponente.COMPARADOR)
        return nodo


    def __verificar_texto(self):
        """
        Verifica si el tipo del componente léxico actuales de tipo TEXTO

        Texto ::= $/\w(\s\w)*)?$
        """
        nodo = NodoÁrbol(TipoNodo.TEXTO, contenido =self.componente_actual)
        self.__verificar_tipo_componente(TipoComponente.TEXTO)
        return nodo

    def __verificar_entero(self):
        """
        Verifica si el tipo del componente léxico actuales de tipo ENTERO

        Entero ::= (-)?\d+
        """
        nodo = NodoÁrbol(TipoNodo.ENTERO, contenido =self.componente_actual)
        self.__verificar_tipo_componente(TipoComponente.ENTERO)
        return nodo


    def __verificar_flotante(self):
        """
        Verifica si el tipo del componente léxico actuales de tipo FLOTANTE

        Flotante ::= (-)?\d+.(-)?\d+
        """
        nodo = NodoÁrbol(TipoNodo.FLOTANTE, contenido =self.componente_actual)
        self.__verificar_tipo_componente(TipoComponente.FLOTANTE)
        return nodo


    def __verificar_identificador(self):
        """
        Verifica si el tipo del componente léxico actuales de tipo
        IDENTIFICADOR

        Identificador ::= [a-zA-Z]([a-zA-Z0-9\_])*
        """
        nodo = NodoÁrbol(TipoNodo.IDENTIFICADOR, contenido =self.componente_actual)
        self.__verificar_tipo_componente(TipoComponente.IDENTIFICADOR)
        return nodo
    

    def __verificar(self, texto_esperado ):

        """
        Verifica si el texto del componente léxico actual corresponde con
        el esperado cómo argumento
        """

        if self.componente_actual.texto != texto_esperado:
            self.manejar_errores()
        elif self.recupendoseError == False:
            self.__pasar_siguiente_componente()


    def __verificar_tipo_componente(self, tipo_esperado ):

        if self.componente_actual.tipo is not tipo_esperado:
            self.manejar_errores()
        elif self.recupendoseError == False:
            self.__pasar_siguiente_componente()


    def __pasar_siguiente_componente(self):
        """
        Pasa al siguiente componente léxico
        """
        self.posición_componente_actual += 1

        if self.posición_componente_actual >= self.cantidad_componentes:
            return

        self.componente_actual = \
                self.componentes_léxicos[self.posición_componente_actual]