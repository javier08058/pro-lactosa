# Archivo principal del Compilador de Prolatosa
# -*- coding: utf-8 -*-

# Importacion de archivos y librerias
from utils import archivos as utils
from explorador.explorador import Explorador 
from analizador.analizador_gramatica import Analizador
from analizador.analizador import Analizador as Arbol
from verificador.verificador import Verificador
from generador.generador import Generador

import argparse

#Parser de acciones para el comando help
parser = argparse.ArgumentParser(description='Interprete para el lenguaje de programacion Prolactosa')

parser.add_argument('--solo-explorar', dest='explorador', action='store_true', 
        help='ejecuta solamente el explorador y retorna una lista de componentes lexicos')

parser.add_argument('--solo-analizar', dest='analizador', action='store_true', 
        help='ejecuta hasta el analizador')

parser.add_argument('--solo-analizar-Arbol', dest='arbol', action='store_true', 
        help='ejecuta hasta el analizador y retorna un preorden del arbol sintáctico')

parser.add_argument('--solo-verificar', dest='verificador', action='store_true', 
        help='ejecuta hasta el verificador y retorna un preorden del arbol sintáctico')

parser.add_argument('--solo-generar', dest='generador', action='store_true', 
        help='ejecuta hasta el generador y retorna el código generado en python')


parser.add_argument('archivo',
        help='Archivo de codigo fuente')

def prolactosa():
    #La funcion agarra los posibles argumentos y revisa cual de las opciones fue seleccionada o hace print al help
    args = parser.parse_args()

    if args.explorador is True: 

        texto = utils.cargar_archivo(args.archivo)
        exp = Explorador(texto)
        exp.explorar()
        if exp.tiene_errores():
            exp.imprimir_errores()
        else:
            exp.imprimir_componentes()

    elif args.analizador is True: 

        texto = utils.cargar_archivo(args.archivo)

        exp = Explorador(texto)
        exp.explorar()
        if exp.tiene_errores():
            exp.imprimir_errores()
        else:
            analizador = Analizador(exp.componentes)
            analizador.analizar()
            if analizador.tiene_errores():
                analizador.imprimir_errores()
            else:
                print('Todo en orden')

    elif args.arbol is True: 
        texto = utils.cargar_archivo(args.archivo)

        exp = Explorador(texto)
        exp.explorar()
        if exp.tiene_errores():
            exp.imprimir_errores()
        else:
            analizador = Arbol(exp.componentes)
            analizador.analizar()
            if analizador.tiene_errores():
                analizador.imprimir_errores()
            else:
                analizador.imprimir_asa()

    elif args.verificador is True: 
        texto = utils.cargar_archivo(args.archivo)

        exp = Explorador(texto)
        exp.explorar()
        if exp.tiene_errores():
            exp.imprimir_errores()
        else:
            analizador = Arbol(exp.componentes)
            analizador.analizar()
            if analizador.tiene_errores():
                analizador.imprimir_errores()
            else:
                verificador = Verificador(analizador.asa)
                verificador.verificar()
                if verificador.tiene_errores():
                    verificador.imprimir_errores()
                else:
                    verificador.imprimir_asa()

    elif args.generador is True:
        texto = utils.cargar_archivo(args.archivo)

        exp = Explorador(texto)
        exp.explorar()
        if exp.tiene_errores():
            exp.imprimir_errores()
        else:
            analizador = Arbol(exp.componentes)
            analizador.analizar()
            if analizador.tiene_errores():
                analizador.imprimir_errores()
            else:
                verificador = Verificador(analizador.asa)
                verificador.verificar()
                if verificador.tiene_errores():
                    verificador.imprimir_errores()
                else:
                    gen = Generador(verificador.asa)
                    gen.generar()
    else:
        parser.print_help()


if __name__ == '__main__':
    prolactosa()
