# Clases para el manejo de un árbol de sintáxis abstracta

from enum import Enum, auto
from explorador.ComponenteLexico import ComponenteLéxico       
import copy

class TipoNodo(Enum):
    """
    Describe el tipo de nodo del árbol
    """
    PROGRAMA              = auto()
    ASIGNACION            = auto()
    IDENTIFICADOR         = auto()
    ENTERO                = auto()
    FLOTANTE              = auto()
    TEXTO                 = auto()
    VERACIDAD             = auto()
    INVOCACION            = auto()
    PARAMETROS_INVOCACION = auto()
    PARAMETROS_FUNCION    = auto()
    EXPRESIÓN_MATEMÁTICA  = auto()
    EXPRESIÓN             = auto()
    OPERADOR              = auto()
    FUNCION               = auto()
    INSTRUCCION           = auto()
    BIFURCACION           = auto()
    SISUCEDE              = auto()
    SINO                  = auto()
    CONDICION             = auto()
    COMPARACION           = auto()
    COMPARADOR            = auto()
    RETORNO               = auto()
    REPETICION            = auto()
    PRINCIPAL             = auto()
    BLOQUE_INSTRUCCIONES  = auto()
    OPERADOR_LOGICO       = auto()
    AMBIENTE_ESTANDAR    = auto()

class NodoÁrbol:

    tipo      : TipoNodo
    contenido : ComponenteLéxico
    atributos : dict
    nodos     : list

    def __init__(self, tipo, contenido = None, nodos = [], atributos = {}):

        self.tipo      = tipo
        self.contenido = contenido
        self.nodos     = nodos
        self.atributos = copy.deepcopy(atributos)
        
    def visitar(self, visitador):
        return visitador.visitar(self)

    def __str__(self):

        # Coloca la información del nodo
        resultado = '{:30}\t'.format(self.tipo)
        
        if self.contenido is not None:
            resultado += '{:10}\t'.format(self.contenido.texto)
        else:
            resultado += '{:10}\t'.format('')


        if self.atributos != {}:
            resultado += '{:38}'.format(str(self.atributos['tipo']))
        else:
            resultado += '{:38}\t'.format('')

        if self.nodos != []:
            resultado += '<'

            # Imprime los tipos de los nodos del nivel siguiente
            for nodo in self.nodos[:-1]:
                if nodo is not None:
                    resultado += '{},'.format(nodo.tipo)

            resultado += '{}'.format(self.nodos[-1].tipo)
            resultado += '>'

        return resultado


class ÁrbolSintáxisAbstracta:

    raiz : NodoÁrbol

    def imprimir_preorden(self):
        self.__preorden(self.raiz)

    def __preorden(self, nodo):

        print(nodo)

        if nodo is not None:
            for nodo in nodo.nodos:
                self.__preorden(nodo)
